package calculatorExam;

/**
 * Класс содержит константы используемые в Калькуляторе
 *
 * @author Artem Zhukov
 */

public abstract class Constants {

    public static final String ICON_PATH = "icon/";
    public static final String ICON_FILE_EXTENSION = ".PNG";
    public static final char POINT = '.';
    public static final char COMMA = ',';
    public static final String EMPTY_STRING = "";
    public static final String EQUALLY = "=";
    public static final String PLUS = "+";
    public static final String MINUS = "-";
    public static final String DIV = "/";
    public static final String MULTI = "*";

}
