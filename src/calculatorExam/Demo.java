package calculatorExam;

import javax.swing.*;

/**
 * Калькулятор. Выполняет простейшие операции
 *
 * @author Artem Zhukov
 */
public class Demo extends JFrame {

    public static void main(String[] args) {
       new Calculator();
    }
}
