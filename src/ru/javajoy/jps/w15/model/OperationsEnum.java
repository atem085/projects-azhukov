package ru.javajoy.jps.w15.model;

/**
 * Перечисление операций
 *
 * @author Artem Zhukov
 */
public enum OperationsEnum {
    COPY,
    MOVE,
    DELETE,
    SIZE
}
