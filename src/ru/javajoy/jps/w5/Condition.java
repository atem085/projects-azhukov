package ru.javajoy.jps.w5;

interface Condition {

    boolean check(String word);

}
