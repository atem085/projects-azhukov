package ru.javajoy.jps.w5;

public class ExceptionCheckCondition extends Exception {

    public ExceptionCheckCondition(String text) {
        super(text);
    }
}
